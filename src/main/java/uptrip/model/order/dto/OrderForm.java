package uptrip.model.order.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class OrderForm {
    private List<OrderProductDto> orderProducts;
    private String totalPrice;
}
